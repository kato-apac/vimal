view: user_pdt {
  parameter: logic_expression {

    type: unquoted
    allowed_value: {
      label: "AND LOGIC"
      value: "AND"
    }
    allowed_value: {
      label: "OR LOGIC"
      value: "OR"
    }
  }
  derived_table: {
    sql: SELECT
          age,
          country,
          city
      FROM users
      WHERE {% condition age%} users.age {% endcondition %} {%parameter logic_expression%} {% condition country%} users.country {% endcondition %}
      GROUP BY
          1,2,3 ;;
  }

  filter: age {
    type: number
  }

  filter: country  {
    type: string
  }
  dimension: city {
    type: string
  }

}
