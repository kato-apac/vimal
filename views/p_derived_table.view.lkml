view: Customer_behaviours {
  derived_table: {
    sql: SELECT
          order_items."USER_ID"  AS "order_items.user_id",
          COUNT(DISTINCT ( order_items."ORDER_ID"  ) ) AS "order_items.no_of_orders",
          MIN(DATE(created_at)) AS first_order_date,
          MAX(DATE(created_at)) AS recent_order_date,
          SUM(sale_price) AS total_sale_price,
      FROM order_items

      GROUP BY
          1, 2
      ORDER BY
      user_id DESC
       ;;
  }

# filter: xxxxxxe {
#   type: number/string
# }

# dimension: country  {
#   type: string
# }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: order_items_user_id {
    type: number
    primary_key: yes
    sql: ${TABLE}."order_items.user_id" ;;
  }

  dimension: sale_prie_tier {
    type:  tier
    tiers: [5, 20, 50, 100, 500, 1000]
    style:  integer
    value_format_name: usd
    sql: ${TABLE}.total_sale_price;;
  }

  dimension: order_items_no_of_orders {
    type: number
    sql: ${TABLE}."order_items.no_of_orders" ;;
  }

  dimension: order_items_created_date {
    type: string
    sql: ${TABLE}."order_items.created_date" ;;
  }

  dimension_group: first_order_date {
    type: time
    timeframes: [date,week,month,year,quarter, raw]
    sql: ${TABLE}.first_order_date;;
  }

  dimension_group: recent_order_date {
    type: time
    timeframes: [date, week, month, year, quarter]
    sql: ${TABLE}.recent_order_date ;;
  }

  measure: average_lifetime_orders {
    type: average
    sql: ${order_items_no_of_orders} ;;
  }

  set: detail {
    fields: [order_items_user_id, order_items_no_of_orders]
  }
}






# SELECT
#           (TO_CHAR(TO_DATE(CONVERT_TIMEZONE('UTC', 'Australia/Sydney', CAST(order_items."CREATED_AT"  AS TIMESTAMP_NTZ))), 'YYYY-MM-DD')) AS "order_items.created_date",
#           order_items."USER_ID"  AS "order_items.user_id"
#       FROM "PUBLIC"."ORDER_ITEMS" AS order_items
#       GROUP BY
#           (TO_DATE(CONVERT_TIMEZONE('UTC', 'Australia/Sydney', CAST(order_items."CREATED_AT"  AS TIMESTAMP_NTZ)))),
#           2
#       ORDER BY
#           1 DESC
