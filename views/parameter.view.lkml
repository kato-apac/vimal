view: parameter {

    parameter: select_timeframe_advanced {
      label: "Select Timeframe"
      type: unquoted
      default_value: "month"
      allowed_value: {
        value: "year"
        label: "Years"
      }
      allowed_value: {
        value: "quarter"
        label: "Quarters"
      }
      allowed_value: {
        value: "month"
        label: "Months"
      }
      allowed_value: {
        value: "week"
        label: "Weeks"
      }
      allowed_value: {
        value: "day"
        label: "Days"
      }
      allowed_value: {
        value: "ytd"
        label: "YTD"
      }
    }

    parameter: select_comparison  {
      label: "Select Comparison Type"
      group_label: "period"
      group_item_label: "year"

      type: unquoted
      default_value: "period"

      allowed_value: {
        label: "Previous Year"
        value: "year"
      }

      allowed_value: {
        label: "Previous Period"
        value: "period"
      }
    }

    parameter: apply_to_date_filter_advanced {
      type: yesno
      default_value: "false"
    }

    parameter: select_reference_date_advanced {
      label: " Select Reference Date"
      description: "Choose any date to compare it with the previous day/week/month/year. Any date during a week/month/year will act as the entire week/month/year"
      type: date
      convert_tz: no
    }


### CURRENT TIMESTAMP {

    dimension_group: current_timestamp_advanced {
      type: time
      hidden: yes
      timeframes: [raw,hour,date,week,month,month_name,month_num,year,hour_of_day,day_of_week_index,day_of_month,day_of_year]
      sql:CURRENT_TIMESTAMP();;#### BIGQUERY
      #### SNOWFLAKE
#     sql: CURRENT_TIMESTAMP() ;;

    }


    dimension: current_timestamp_month_of_quarter_advanced {
      type: number
      hidden: yes
      sql:
      CASE
        WHEN ${current_timestamp_advanced_month_num} IN (1,4,7,10) THEN 1
        WHEN ${current_timestamp_advanced_month_num} IN (2,5,8,11) THEN 2
        ELSE 3
      END
    ;;
    }

    dimension_group: selected_reference_date_default_today_advanced {
      description: "This Dimension will make sure that when \"Select Reference date\" is set in  the future then we use the current day for reference"
      hidden: yes
      type: time
      convert_tz: no
      datatype: date
      timeframes: [raw,date,day_of_month,day_of_week,day_of_week_index,day_of_year,week, week_of_year, month, month_name, month_num, quarter, quarter_of_year, year]
      sql:
      case
        when {% parameter parameters.select_reference_date_advanced %} is null or ${parameters.current_timestamp_advanced_date} <= date({% parameter parameters.select_reference_date_advanced %})
          then ${parameters.current_timestamp_advanced_date}
        else date({% parameter parameters.select_reference_date_advanced %})
      end
    ;;

### CURRENT TIMESTAMP }
      }
  # # You can specify the table name if it's different from the view name:
  # sql_table_name: my_schema_name.tester ;;
  #
  # # Define your dimensions and measures here, like this:
  # dimension: user_id {
  #   description: "Unique ID for each user that has ordered"
  #   type: number
  #   sql: ${TABLE}.user_id ;;
  # }
  #
  # dimension: lifetime_orders {
  #   description: "The total number of orders for each user"
  #   type: number
  #   sql: ${TABLE}.lifetime_orders ;;
  # }
  #
  # dimension_group: most_recent_purchase {
  #   description: "The date when each user last ordered"
  #   type: time
  #   timeframes: [date, week, month, year]
  #   sql: ${TABLE}.most_recent_purchase_at ;;
  # }
  #
  # measure: total_lifetime_orders {
  #   description: "Use this for counting lifetime orders across many users"
  #   type: sum
  #   sql: ${lifetime_orders} ;;
  # }
}

# view: parameter {
#   # Or, you could make this view a derived table, like this:
#   derived_table: {
#     sql: SELECT
#         user_id as user_id
#         , COUNT(*) as lifetime_orders
#         , MAX(orders.created_at) as most_recent_purchase_at
#       FROM orders
#       GROUP BY user_id
#       ;;
#   }
#
#   # Define your dimensions and measures here, like this:
#   dimension: user_id {
#     description: "Unique ID for each user that has ordered"
#     type: number
#     sql: ${TABLE}.user_id ;;
#   }
#
#   dimension: lifetime_orders {
#     description: "The total number of orders for each user"
#     type: number
#     sql: ${TABLE}.lifetime_orders ;;
#   }
#
#   dimension_group: most_recent_purchase {
#     description: "The date when each user last ordered"
#     type: time
#     timeframes: [date, week, month, year]
#     sql: ${TABLE}.most_recent_purchase_at ;;
#   }
#
#   measure: total_lifetime_orders {
#     description: "Use this for counting lifetime orders across many users"
#     type: sum
#     sql: ${lifetime_orders} ;;
#   }
# }
