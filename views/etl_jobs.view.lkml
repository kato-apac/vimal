include: "events.view"
view: etl_jobs {
  extends: [events]
  sql_table_name: "PUBLIC"."ETL_JOBS"
    ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}."ID" ;;
  }

dimension: ip {
  type: string
  sql:  ${ip_address} ;;
}

  dimension_group: completed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."COMPLETED_AT" ;;
  }

  measure: count {
    type: count
    drill_fields: [id]
  }
}
