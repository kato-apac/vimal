view: order_items {
  sql_table_name: "PUBLIC"."ORDER_ITEMS";;
  drill_fields: [id]
# comment
  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}."ID" ;;
  }
# comment
  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."CREATED_AT" ;;
  }

  parameter: date_granularity {
    type: unquoted
    allowed_value: {
      label: "Weeks"
      value: "week"
    }
    allowed_value: {
      label: "Months"
      value: "month"
    }
    allowed_value: {
      label: "Years"
      value: "year"
    }
  }

  dimension: date {
   label: "week"
    sql:
    {% if date_granularity._parameter_value  == 'week'%}  ${created_week}

    {% elsif date_granularity._parameter_value  == 'month'%}  ${created_month}

    {% elsif date_granularity._parameter_value  == 'year'%}  ${created_year}

    {% else %}
      ${created_date}
    {% endif %};;
  }

  dimension_group: delivered {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."DELIVERED_AT" ;;
  }

  dimension: inventory_item_id {
    type: number
    # hidden: yes
    sql: ${TABLE}."INVENTORY_ITEM_ID" ;;
  }

  dimension: product_header {
    type: string
    ### The below SQL is just used to add html tag ###
    sql: 'null' ;;
    html: <table style="border: 0px solid black; width: 100%">
    <tr>
    <td>
    <div style="background-color: white; font-size: 35px; font-weight: normal; font-family: Open Sans, Helvetica, Arial, sans-serif;" align="left">Product</div>
    </td>
    <td style="text-align: right;">
    <div><img align="right" src="https://livingedge.com.au/on/demandware.static/-/Library-Sites-shared_library/default/dw74b84ace/logo.svg" width="130" height="150" /></div>
    </td>
    </tr>
    </table> ;;
    }

  dimension: order_id {
    type: number
    sql: ${TABLE}."ORDER_ID" ;;
    action: {
      label: "send a message"
      url: "https://hooks.slack.com/services/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"
      # form_url: "https://app.slack.com/client/T01G9CW00F7/C025HG01BPE/help/form.json"
      param: {
        name: "Date"
        value: "https://katosolutions.au.looker.com/dashboards/329?Date%20Granularity=week"
      }

      form_param: {
        name: "Text"
        type: textarea
        default: "Hey, Please check this order {{value}} out"
      }

      form_param: {
        name: "Members"
        type: select
        default: "Vimal"
        option: {
          name: "Vimal"
          label: "Vimal"
        }
        option: {
          name:"Jyothish Poduval"
          label: "Jyothish Poduval"
          }
          option: {
            name: "Varsha Ramesh"
            label: "Varsha Ramesh"
          }
      }

      form_param: {
        name: "Channels"
        type: select
        default: "kato-all-team-members"
        option: {
          name: "kato-all-team-members"
        }
        option: {
          name:"slack-api"
        }
      }
  }
}

  dimension_group: returned {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."RETURNED_AT" ;;
  }

  dimension: returned_yes_no{
    case: {
      when:{
        sql: ${returned_date} IS NOT NULL;;
        label: "returned"
        }
      when: {
        sql: ${returned_date} IS NULL;;
        label: "not returned"
        }
  }
  }
  dimension: sale_price {
    type: number
    sql: ${TABLE}."SALE_PRICE" ;;
    value_format_name: usd
  }


  dimension: sale_price_tier {
    type:  tier
    tiers: [5, 20, 50, 100, 500, 1000]
    style:  integer
    sql: ${sale_price};;
    value_format: "0.##"
  }

  dimension_group: shipped {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}."SHIPPED_AT" ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}."STATUS" ;;
  }

  dimension: user_id {
    type: number
    # hidden: yes
    sql: ${TABLE}."USER_ID" ;;
  }

  measure: no_of_orders {
    type: count_distinct
    sql: ${order_id} ;;
    drill_fields: [order_id]
  }



  dimension: order_tier {
    type: tier
    tiers: [1,2,3,6,10]
    style: integer
    sql: ${order_id} ;;
  }

  dimension: is_customer_active {
    type: yesno
    sql:  ;;
  }

measure: first_order_date {
  type: max
  sql: ${created_date} ;;
}

measure: recent_date_order {
  type: min
  sql: ${created_date} ;;
}

  measure: Total_Cost{
    type: sum
    value_format_name: usd
    filters: [status: "Complete"]
    sql: ${sale_price} ;;
  }

# dimension: Gross_Margin {
#     type: number
#     value_format_name: usd
#     sql: ${Total_Gross_Revenue} - ${Total_Cost} ;;
#   }

#   dimension: Average_Gross_Margin {
#     type: number
#     value_format_name: usd
#     sql: ${Total_Gross_Revenue}/ ${Total_Cost} ;;
# }

  measure: Total_Gross_Revenue {
  type: sum
  value_format_name: usd
  filters: [status: "-Returned", status: "-Cancelled" ]
  sql:${sale_price};;
  group_label: "Measures"
  }

  measure: Average_cost {
    type: average
    value_format_name: usd
    filters: [status: "Complete"]
    sql: ${sale_price} ;;
    group_label: "Measures"
  }

  measure: returned_items {
    type: count_distinct
    sql:${inventory_item_id} ;;
    filters: [status: "Returned"]
    group_label: "Measures"
  }

  measure: returned_item_rate {
    type: percent_of_total
    sql: ${returned_items} - ${order_items.count} ;;
    group_label: "Measures"
  }

  # measure: Total_Gross_Margin {
  #   type: number
  #   value_format_name: usd
  #   sql: ${Total_Gross_Revenue}-${inventory_items.Total_cost} ;;
  #   drill_fields: [products.category , products.brand]
  #   group_label: "Measures"
  # }
# #Total Gross Margin Amount / Total Gross Revenue
#   measure: Total_Gross_Margin_percent{
#     type: number
#     sql: (${Total_Gross_Margin}/${Total_Gross_Revenue}) *100 ;;
#     group_label: "Measures"
#   }

measure: total_revenue_percent {
  type: number
  sql: (${Total_Gross_Revenue}/${Total_Sale_Price}) *100 ;;
  group_label: "Measures"
}

 measure: Cumulative_Total_Sales {
  type: running_total
  sql: ${Total_Sale_Price}  ;;
  value_format_name: usd
  group_label: "Measures"
 }
  measure: Total_Sale_Price{
    type: sum
    sql: ${sale_price} ;;
    value_format_name: usd
    group_label: "Measures"
  }

  measure: Average_Sale_Price {
    type: average
    sql: ${sale_price} ;;
    value_format_name: usd
    group_label: "Measures"
  }

measure: No_of_Customers {
  type: count_distinct
  sql: ${user_id} ;;
  group_label: "Measures"
}

measure: Average_Spend_per_Customer {
  type: number
  value_format_name: usd
  sql: ${Total_Sale_Price}/${No_of_Customers} ;;
  group_label: "Measures"
}

  measure: count {
    type: count
    drill_fields: [detail*]
    group_label: "Measures"
  }
measure: no_of_ordersitems {
  type: count_distinct
  sql: ${order_id} ;;
}

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      inventory_items.product_name,
      inventory_items.id,
      users.last_name,
      users.id,
      users.first_name
    ]
  }
}
