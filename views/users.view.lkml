# include: "events.view"
view: users {
  # extends: [events]
  sql_table_name: "PUBLIC"."USERS" ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}."ID" ;;
  }

  dimension: age {
    type: number
    sql: ${TABLE}."AGE" ;;
  }

  dimension: city {
    type: string
    sql: ${TABLE}."CITY" ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}."COUNTRY" ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year, day_of_month
    ]
    sql: ${TABLE}."CREATED_AT" ;;
  }

  dimension: email {
    type: string
    sql: ${TABLE}."EMAIL" ;;
  }

  dimension: first_name {
    type: string
    sql: initcap(${TABLE}."FIRST_NAME") ;;
  }

  dimension: gender {
    type: string
    sql: ${TABLE}."GENDER" ;;
  }

dimension:  full_name{
  type: string
  sql: ${first_name} || ' ' || ${last_name} ;;
  action: {
    label: "send contact details"
    url: "https://katosolutions.com/contact/"
    icon_url: "https://katosolutionscom.files.wordpress.com/2019/12/kato-solutions-side-logo-400px.png"
    # form_url: "https://katosolutions.com/contact/{{value}}form.json"
    form_param: {
      name: "Name (required)"
      type: string
      label: "your name"
      required: yes
      }

      form_param: {
        name: "Company (required)"
        type: string
        label: "Company name"
        required: yes
        }

        form_param: {
          name: "Email (required)"
          type: string
          label: "Company Email"
          required: yes
          }

          form_param: {
            name: "Phone (required)"
            type: string
            label: "Phone number"
            required: yes
            }

            form_param: {
              name: "Message (required)"
              type: string
              label: "hi"
              }
  }
}

  dimension: last_name {
    type: string
    sql:initcap( ${TABLE}."LAST_NAME" );;
  }

  dimension: latitude {
    type: number
    sql: ${TABLE}."LATITUDE" ;;
  }

  dimension: longitude {
    type: number
    sql: ${TABLE}."LONGITUDE" ;;
  }

  dimension: user_location {
    type: location
    sql_latitude: ${latitude};;
    sql_longitude: ${longitude} ;;
  }


  dimension: state {
    type: string
    sql: ${TABLE}."STATE" ;;
  }
#this is comment
  dimension: traffic_source {
    type: string
    sql: ${TABLE}."TRAFFIC_SOURCE" ;;
    drill_fields: [details*]
  }

  set: details {
    fields: [age_tier, gender, user_name]
  }

  dimension: zip {
    type: zipcode
    sql: ${TABLE}."ZIP" ;;
  }
  dimension: user_name {
    type: string
    sql: ${first_name} || ' ' || ${last_name};;
  }
  dimension: from_ca_or_ny {
    type: yesno
    sql:  ${state} = 'California' OR ${state} = 'New York';;
  }

  dimension: age_tier {
    type: tier
    tiers: [15,26,36,51,66]
    style: integer
    sql: ${age} ;;
  }

measure: unique_customers {
  type: count_distinct
  sql: ${id} ;;
}

dimension: New_Customers {
  type: yesno
#  sql: ${user_signup_date} < 90 ;;
  sql: DATEDIFF(day, ${created_raw}, CURRENT_DATE()) < 90 ;;
}

# dimension: user_signup_date {
#   type: date
#   sql: ${created_date} ;;
# }

  dimension_group: time_since_signup {
    type: duration
    intervals: [day,month]
    sql_start: ${created_raw} ;;
    sql_end: (SELECT CURRENT_DATE()) ;;
  }


dimension: current_date {
  type: date
  sql: CURRENT_DATE;;
  allow_fill: yes
}

# dimension: is_active{
#   type: yesno
#   sql: (${current_date} - ${user_signup_date}) < 90 ;;
# }

  measure: avg_num_days_since_signup {
    type: average
    value_format: "######"
    sql: ${days_time_since_signup} ;;
  }

  measure: avg_num_months_since_signup {
    type: average
    value_format: "######"
    sql: ${months_time_since_signup} ;;
  }

  measure: count {
    type: count
    drill_fields: [id, last_name, first_name, events.count, order_items.count]
  }
  measure: average_age {
    type: average
    sql: ${age} ;;
  }
}
